
https://mjml.io

1. `npm i` to install dependencies
2. `npm start` to watch and compile to html 
3. `npm run build` to compile the final minified html

More on the CLI https://mjml.io/documentation/#command-line-interface

Note: All [their documentation](https://mjml.io/download ) seems to be for Mac, made the scripts ^ so it's easier to run in Windows.